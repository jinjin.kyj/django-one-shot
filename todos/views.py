from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm

# Create your views here.s


def todo_list_list(request):
    list = TodoList.objects.all()
    context = {"todo_list_list": list}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    item = get_object_or_404(TodoList, id=id)
    context = {"list_details": item}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    edit = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoForm(request.POST, instance=edit)
        if form.is_valid():
            edit = form.save()
            return redirect("todo_list_detail", id=edit.id)
    else:
        form = TodoForm(instance=edit)

    context = {
        "form": form,
    }

    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    delete = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/item_create.html", context)


def todo_item_update(request, id):
    edit = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":
        form = ItemForm(request.POST, instance=edit)
        if form.is_valid():
            edit = form.save()
            return redirect("todo_list_detail", id=edit.list.id)
    else:
        form = ItemForm(instance=edit)

    context = {
        "form": form,
    }

    return render(request, "todos/item_update.html", context)
